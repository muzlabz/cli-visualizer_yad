#### CLI-VISUALIZER tweaker. Yad version

```


███╗   ███╗██╗   ██╗███████╗██╗      █████╗ ██████╗ ███████╗
████╗ ████║██║   ██║╚══███╔╝██║     ██╔══██╗██╔══██╗╚══███╔╝
██╔████╔██║██║   ██║  ███╔╝ ██║     ███████║██████╔╝  ███╔╝ 
██║╚██╔╝██║██║   ██║ ███╔╝  ██║     ██╔══██║██╔══██╗ ███╔╝  
██║ ╚═╝ ██║╚██████╔╝███████╗███████╗██║  ██║██████╔╝███████╗
╚═╝     ╚═╝ ╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝╚═════╝ ╚══════╝
                                                            
█████╗ █████╗ █████╗ █████╗ █████╗ █████╗ █████╗ █████╗ █████╗
╚════╝ ╚════╝ ╚════╝ ╚════╝ ╚════╝ ╚════╝ ╚════╝ ╚════╝ ╚════╝


```


### [Mabox jgmenu version](https://gitlab.com/muzlabz/colorizer_vis_mabox) !!!



# This version is in development. It runs fine. 

## Todo

        - rewrite to yad html - to make the gui more dynamic.
        (need to learn this)
        - add mono gradient function
        - reverse colors function
        - replace function script to one.
        - replace function mb-setvar 
 
### This settings are the real basics. 

## For more options read config file for extra values or add missing ones.

### Developed with and for Mabox... 

- Possible to run on other linux systems.

##### The application utilizes YAD to set basic parameters for VIS (audio visualizer). 


![menu](images/menu.png){width=410px}  ![pick](images/yad_colorizer.png){width=300px} 

---

### Launch `vis-manager.sh` to change the `vis` variable. 

> __hotkey toggle:__ s = mono or stereo mode

---

### Use `Install` for the easy way.

_(Uninstall script only bin and .desktop files.  Configs need to be removed manualy)_

---

### Manual Installation. 

```
yay -S cli-visualizer terminator yad wmctrl
```

### Create the necessary directories and place the configuration files in the following locations:

Ensure scripts are executable:

```
chmod +x bin/*
```

Copy script to bin.

```
cp bin/* $HOME/bin
```

Copy config files.

```
$ cp .local/share/applications/* $HOME/.local/share/applications
```

```
$ cp .config/vis/* $HOME/.config/vis  
```

```
$ cp .config/terminator/* $HOME/.config/terminator 
```

#### Configure hotkey:

##### Add the following to ~/.config/openbox/rc.xml to bind a key to start transparent-vis.sh:

Two options for the size :  -full size or -half size width. *(default = full : height 200)*

```
transparent-vis.sh 500 half
```
    
```
<keybind key="C-A-v">
  <action name="Execute">
    <execute>transparent-vis.sh 200 half</execute>
  </action>
</keybind>
```

### Ensure Picom compositor excludes shadow for transparency:

```
{ 		match = 
		" name = 'vistransparent'";
		shadow = false;      	
		opacity = 1.0;
		blur-background = false;
},
```

---

###### FOR NON MABOX USERS : COPY `MB_SETVAR` TO HOME/bin

`$HOME/bin/mb-setvar` _Note: tool to change values of config files._ 

(I put it in, only for `NON Mabox` systems) creator @napcok. 

---
> #### Trouble shoot: 
> ##### `Issue:`  Value's are not changing when i `save the settings`. 
> ##### `Awnser:` Look at `mb-vis-config`. Remove `#` from the needed line(s), to be able to change the value.

---

![theme](images/theme.png){width=270px} ![stereo1](images/stereo-one.gif){width=295px}
