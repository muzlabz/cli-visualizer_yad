#!/bin/bash
# vis-yad-colors.sh

DIR="$HOME/.config/vis"
WELCOME="$DIR/welcome-vis.txt"
COLOR_DIR="$DIR/colors"
FILE_NAME="custom_colors"

welcome="$WELCOME"

notify-send -i games-config-custom -t 500000 -- "HOW TO CREATE COLOR SCHEME." "<i>$(cat $welcome)</i>"


	


while true; do
    color=$(yad --title="Add Color" \
        --color \
        --text="Select a color:" \
        --button="Ok:0" --button="Save:2" --button="Cancel:1")
	
    exit_status=$?

    if [ $exit_status -eq 0 ]; then
        echo "$color" >> "/tmp/$FILE_NAME"
    elif [ $exit_status -eq 2 ]; then
        if [ $(wc -l < "/tmp/$FILE_NAME") -gt 0 ]; then
            new_name=$(yad --title="Save Colors" \
                --entry \
                --text="Enter file name:" \
                --button="Ok:0" --button="Cancel:1" \
				--center --width="350")

            if [ ! -z "$new_name" ]; then
                cp "/tmp/$FILE_NAME" "$COLOR_DIR/$new_name"
                rm "/tmp/$FILE_NAME"
                yad --title="Success" --text="File saved as $new_name." --button="Ok:0" --center --width="350"
            else
                rm "/tmp/$FILE_NAME"
                yad --title="Error" --text="File name cannot be empty." --button="Ok:0" --center --width="350"
            fi
        else
            yad --title="Error" --text="No colors selected." --button="Ok:0" --center --width="350"
        fi
    else
        break
    fi
done
